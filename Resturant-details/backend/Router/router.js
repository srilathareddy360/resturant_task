//importing external dependencies
import express from "express";

//importing internal dependencies
//importing validations
import { Resturant_details_validator} from "../middlewares/validations/resturant_details_validation.js"
import {Delete_validator} from "../middlewares/validations/delete_Validation.js";

//import path handlers
import {delete_Resturant_details } from "../controllers/delete_handler.js";
import { Restuarant_information_handler } from "../controllers/resturant_details_handler.js";
import {Get_Resturant_details} from "../controllers/get_handler.js";
import {Update_resturant_details} from "../controllers/put_handler.js";

const router = express.Router();



/****************API CALLS******************** */

/**********POST***************** */
router.post("/resturant_details", Resturant_details_validator,Restuarant_information_handler)


/****************DELETE********** */
router.delete("/delete_resturant/:Restaurant_name ",delete_Resturant_details )

/***************GET*************** */
router.get("/get_resturant_details",Get_Resturant_details)


router.put('/update',Update_resturant_details)




export default router;
