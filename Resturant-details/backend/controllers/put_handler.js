import { ResturantModel } from "../models/model.js";



/**This function is used for updatating details based on manager name
 * 
 * @param {*} req express request object
 * @param {*} res express response object
 */
const Update_resturant_details=async( req, res) =>{

    try{
  //updateOne function is used to update details using try catch statements
    const data=await ResturantModel.updateOne({Manager_name: req.body.Manager_name})

    res.status(200).json(data);
    res.send({message:"Updated Sucessfully"})

     } catch(error){

        console.log(error)

        res.status(404).json({ message: error.message });

    }

}



export { Update_resturant_details}