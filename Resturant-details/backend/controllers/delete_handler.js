//import ResturantModel 
import { ResturantModel} from "../models/model.js"

/**
 * This is a  function to soft delete the resturant_details
 * @param request express request object
 * @param response express response object
 * @returns An object containing result or error messages.
 */
 const delete_Resturant_details=async( req, res) =>{

  try{

  const data=await ResturantModel.deleteOne({Manager_name: req.body.Manager_name})

  res.status(200).json(data);

   } catch(error){

      console.log(error)

      res.status(404).json({ message: error.message });

  }

}

export { delete_Resturant_details }