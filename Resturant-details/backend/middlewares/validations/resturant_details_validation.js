
//import all checkers from helper functions
import {
  _name_checker,
  _mobile_no_checker,
  _Timings_checker,
  _Menu_checker,
  _return_object_keys,
} from "./helper_functions.js";

/**
 * This is the register_handler function to handle the parameter validations
 * for the '/register' call.
 */
const Resturant_details_validator = (req, res, next) => {
  // get all parameters required for the register call  Restaurant_name,  Manager_name,  contact_number, Menu,  Timings
  const { Restaurant_name,  Manager_name,  contact_number, Menu,  Timings} = req.body;
  // checking if everything is okay with all params
  const error_object = {
    
    ... _name_checker(Restaurant_name),
    ... _name_checker(Manager_name),
    ... _mobile_no_checker(contact_number),
    ..._Menu_checker(Menu) ,
    ..._Timings_checker(Timings)
   
   
  };

  if (_return_object_keys(error_object).length > 0) {
    res.status(400).send({err: error_object});
  } else {
    next();
  }
};

export { Resturant_details_validator };
